
# TimeOff.Management

Clone from original source https://github.com/timeoff-management/timeoff-management-application

Update the Dockerfile for the build stage.

Add .gitlab-ci.yaml for the pipeline.

Before building, add Node.js, Python 3, and SQLite to the environment. The built image will be stored in the GitLab Container Registry.